# README #

Folders:

* releases - binary versions for windows and Mac
* screenshots - screenshots and gameplay
* PudimAmassadoSrc - Unity 5.1 project folder

# How to play #

Character moves slowly forward. While at that speed you keep staring inside the box where the Pudim is alive. 

* Forward - run
* Space or Y on Xbox controller - jump or double jump

However, if you run or jump you close the box and the Pudim is alive and dead at the same time until you walk slowly or finish the level. If the Pudim dies or you reach the end, it's game over.