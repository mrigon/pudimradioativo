﻿using UnityEngine;
using System.Collections;

public class FlowerCollectable : MonoBehaviour {
    public FlowerCounter counter;

    void OnTriggerEnter2D(Collider2D other) {
        if (other.CompareTag("Player")) {
            counter.addFlower();
            Destroy(this.gameObject);
        }
    }
}
