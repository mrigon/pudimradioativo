﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

[RequireComponent(typeof(Image))]
public class BoxInterfaceController : MonoBehaviour {
    private Image boxImageHolder;
    private float currentDanger;
    private bool _boxState = true;

    public Sprite openBox;
    public Sprite closedBox;
    public Image dangerBar;
    public Image staminaBar;

    public float dangerIncrease = 0.02f;

    
	void Start () {
        this.boxImageHolder = this.GetComponent<Image>();
        this.currentDanger = 0.001f;
    }

    void FixedUpdate() {
        if (!this._boxState) {
            this.currentDanger += this.dangerIncrease * Time.deltaTime;
            this.setDangerLevel((this.currentDanger *4 > 1.0f) ? 100f : this.currentDanger * 400f);
        }
    }

    public bool boxState {
        set {
            if (value != _boxState)
                this._boxState = value;
            else
                return;

            if (_boxState) {
                //Change the image for the openned box
                this.boxImageHolder.sprite = this.openBox;
                // check if the pudim is amassado
                var rand = Random.value;
                Debug.Log(this.currentDanger);
                Debug.Log(rand);
                if (rand < this.currentDanger) {
                    //died
                    //TODO: ending
                    this.setDangerLevel(100f);
                    GameObject.FindGameObjectWithTag("Player").GetComponent<InfiniteWalkerController>().DieAndBeSilent();
                } else {
                    this.currentDanger = 0.001f;
                    this.setDangerLevel(this.currentDanger * 100f);
                }
            } else
                this.boxImageHolder.sprite = this.closedBox;
        }
        get {
            return _boxState;
        }
    }

    public void setDangerLevel(float newSize) {
        Vector2 size = this.dangerBar.rectTransform.sizeDelta;
        size.x = newSize;
        this.dangerBar.rectTransform.sizeDelta = size;
    }

    public void setStaminaLevel(float newSize) {
        Vector2 size = this.staminaBar.rectTransform.sizeDelta;
        size.x = newSize;
        this.staminaBar.rectTransform.sizeDelta = size;
    }
}
