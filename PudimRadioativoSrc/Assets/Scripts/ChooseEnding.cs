﻿using UnityEngine;
using System.Collections;

public class ChooseEnding : MonoBehaviour {
    public GameObject victory;
    public GameObject defeat;
    
    void Start () {
	    if (PlayerPrefs.GetInt("GameOver", 0) == 1) {
            this.victory.SetActive(true);
        } else {
            this.defeat.SetActive(true);
        }
	}
}
