﻿using UnityEngine;
using System.Collections;

public class PortalChooser : MonoBehaviour {
    public bool lastLevel = false;
    public string levelToLoad = "GameOver";

    void OnTriggerEnter2D(Collider2D other) {
        if (other.CompareTag("Player")) {
            if (lastLevel)
                other.GetComponent<InfiniteWalkerController>().DieAndBeSilent(true);
            else
                Application.LoadLevel(levelToLoad);
        }
    }

}
