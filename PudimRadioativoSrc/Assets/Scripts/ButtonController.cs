﻿using UnityEngine;
using System.Collections;

public class ButtonController : MonoBehaviour {
    public GameWindowTransition gameTransition;
    public string levelToLoad = "Main";
    public string controllerButton = "";

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	    if (!controllerButton.Equals("")) {
            if (Input.GetButtonDown(controllerButton))
                onClickButton();
        }
	}

    public void onClickButton() {
        if (this.gameTransition != null) {
            this.gameTransition.startTransition(levelToLoad);
            this.enabled = false;
        } else {
            Application.LoadLevel(this.levelToLoad);
        }
    }
}
