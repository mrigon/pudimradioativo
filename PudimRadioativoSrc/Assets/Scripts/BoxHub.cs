﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(SpriteRenderer))]
public class BoxHub : MonoBehaviour {
    private SpriteRenderer sptRenderer;
    public BoxInterfaceController bic;
    public Sprite openBoxSprite;
    public Sprite closedBoxSprite;
	
    void Start() {
        this.sptRenderer = this.GetComponent<SpriteRenderer>();
    }


	void FixedUpdate () {
	    if (bic.boxState) {
            this.sptRenderer.sprite = this.openBoxSprite;
        } else {
            this.sptRenderer.sprite = this.closedBoxSprite;
        }
	}
}
