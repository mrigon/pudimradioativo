﻿using UnityEngine;
using System.Collections;

public class KillPlayer : MonoBehaviour {
    void OnTriggerEnter2D(Collider2D other) {
        if (other.CompareTag("Player")) {
            Debug.Log("Should end!");
            other.gameObject.GetComponent<InfiniteWalkerController>().DieAndBeSilent();
        }
    }
}
