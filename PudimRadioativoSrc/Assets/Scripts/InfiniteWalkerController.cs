﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Animator), typeof(Rigidbody2D), typeof(AudioSource))]
public class InfiniteWalkerController : MonoBehaviour {
    private Animator charAnimator;
    private Rigidbody2D body2D;
    private AudioSource playerSound;
    private bool isGrounded = false;
    private bool isJumping = false;
    private bool doubleJumped = false;
    private bool doDoubleJump = false;
    private float speed = 0f;
    private float currentStamina;

    public AudioClip looseTransitionSound;
    public AudioClip winTransitionSound;
    public AudioClip jumpSound;
    public AudioClip hitGroundSound;
    public AudioClip stepSound;
    public GameWindowTransition transition;

    public BoxInterfaceController boxReference;
    public Transform groundcheck;
    public float baseMoveSpeed = 0.5f;
    public float moveSpeedIncrement = 1.5f;
    public Vector2 jumpForce;

    public float maxStamina = 100f;
    public float staminaConsumption = 10f;
    public float staminaRecovery = 7f;

    // Use this for initialization
    void Start () {
        this.charAnimator = this.GetComponent<Animator>();
        this.body2D = this.GetComponent<Rigidbody2D>();
        this.playerSound = this.GetComponent<AudioSource>();
        this.currentStamina = this.maxStamina;
        PlayerPrefs.SetInt("GameOver", 0);
        PlayerPrefs.Save();
	}
	
	// Update is called once per frame
	void Update () {
        this.speed = baseMoveSpeed;
        if (Input.GetAxis("Horizontal") > 0.2 && this.isGrounded) {
            if (this.currentStamina > 0f) {
                this.charAnimator.speed = 1.2f;
                this.speed += moveSpeedIncrement * Input.GetAxis("Horizontal");
                this.currentStamina -= this.staminaConsumption * Time.deltaTime;
                this.boxReference.boxState = false;
                if (this.currentStamina < 0f)
                    this.currentStamina = 0f;
            } else {
                if (!this.isJumping)
                    this.boxReference.boxState = true;
                this.currentStamina += this.staminaRecovery * Time.deltaTime;
            }
        } else {
            this.charAnimator.speed = 1f;
            if (!this.isJumping)
                this.boxReference.boxState = true;
            this.currentStamina += this.staminaRecovery * Time.deltaTime;
        }

        if (this.isJumping) {
            this.boxReference.boxState = false;
            this.currentStamina -= this.staminaConsumption * Time.deltaTime;
        }

        if (this.currentStamina > this.maxStamina)
            this.currentStamina = this.maxStamina;
        else if (this.currentStamina < 0f)
            this.currentStamina = 0f;

        this.boxReference.setStaminaLevel(this.currentStamina/this.maxStamina * 100f);

        charAnimator.SetFloat("speed", this.speed);

        if (this.isGrounded || !this.doubleJumped) {
            if (Input.GetButtonDown("Jump") || Input.GetMouseButtonDown(0)) {
                if (this.isGrounded) {
                    this.isJumping = true;
                    this.isGrounded = true;
                } else {
                    this.doDoubleJump = true;
                    this.doubleJumped = true;
                }

                this.playerSound.Stop();
                if (this.jumpSound != null)
                    this.playerSound.PlayOneShot(this.jumpSound);
                charAnimator.SetBool("jump", true);
                return;
            }
        }
    }

    void FixedUpdate() {
        Vector3 pos = this.transform.position;
        this.transform.position = pos + Vector3.right * speed * Time.deltaTime;

        if ((this.isJumping && this.isGrounded)||this.doDoubleJump) {
            //Just started jumping
            //Debug.Log("Jumping!");
            this.isGrounded = false;
            Vector2 actualJumpForce = jumpForce;
            if (Input.GetAxis("Horizontal") > 0.2)
                actualJumpForce += Vector2.right * 50f;
            this.body2D.AddForce(actualJumpForce/ (this.body2D.velocity.y >1f? this.body2D.velocity.y : 1f));
            if (this.doDoubleJump)
                this.doDoubleJump = false;
            return;
        }

        if (this.body2D.velocity.y < 0f) {
            //falling
            this.isGrounded = false;

            // The player is grounded if a circlecast to the groundcheck position hits anything designated as ground
            // This can be done using layers instead but Sample Assets will not overwrite your project settings.
            Collider2D[] colliders = Physics2D.OverlapCircleAll(this.groundcheck.position, 0.2f, 1 << LayerMask.NameToLayer("Ground"));
            for (int i = 0; i < colliders.Length; i++) {
                if (colliders[i].gameObject != gameObject) {
                    this.isGrounded = true;
                    //Debug.Log("Back to ground!");
                    this.charAnimator.SetBool("jump", false);
                    this.isJumping = false;
                    this.doubleJumped = false;
                    this.doDoubleJump = false;

                    if (this.hitGroundSound != null)
                        this.playerSound.PlayOneShot(this.hitGroundSound);
                }
            }
        }

        if (this.isGrounded && !this.playerSound.isPlaying) {
            this.playerSound.loop = true;
            if (this.stepSound != null) {
                this.playerSound.clip = this.stepSound;
                this.playerSound.Play();
            }
        }
    }

    public void DieAndBeSilent(bool victory=false) {
        this.enabled = false;

        PlayerPrefs.SetInt("GameOver", (victory? 1 : 0));
        PlayerPrefs.Save();

        this.transition.transitionSound = victory ? this.winTransitionSound : this.looseTransitionSound;
        this.transition.startTransition("GameOver");
        
        Debug.Log("Game Over!");
        //Application.LoadLevel("GameOver");
    }
}
