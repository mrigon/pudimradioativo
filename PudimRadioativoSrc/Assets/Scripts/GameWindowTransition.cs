﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

[RequireComponent(typeof(RawImage), typeof(AudioSource))]
public class GameWindowTransition : MonoBehaviour {
    private RawImage screenOver;
    private AudioSource audioSource;
    private float timeCounter = 0f;
    private bool isTransition = false;
    private string levelToLoad = "";

    public AudioClip transitionSound;
    public Color transitionColor;
    public float transitionTime = 1f;
    
	void Start () {
        this.screenOver = this.GetComponent<RawImage>();
        this.audioSource = this.GetComponent<AudioSource>();
	}
	
	void Update () {
	    if (isTransition) {
            this.timeCounter += Time.deltaTime;
            this.screenOver.color = Color.Lerp(this.screenOver.color, this.transitionColor, (this.timeCounter/ this.transitionTime) );

            if (this.timeCounter >= this.transitionTime) {
                Application.LoadLevel(this.levelToLoad);
            }
        }
	}

    public void startTransition(string levelName) {
        this.levelToLoad = levelName;
        this.isTransition = true;
        this.screenOver.enabled = true;
        this.timeCounter = 0f;
        if (this.transitionSound != null) { 
            this.transitionTime = Mathf.Max(this.transitionTime, transitionSound.length);
            this.audioSource.PlayOneShot(this.transitionSound);
        }
    }
}
