﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class FlowerCounter : MonoBehaviour {
    private int currentCount = 0;

    public Text flowerLabel;

	// Use this for initialization
	void Start () {
	    if (Application.loadedLevelName == "Level01") {
            PlayerPrefs.SetInt("FlowerCount", 0);
            PlayerPrefs.Save();
        } else {
            this.currentCount = PlayerPrefs.GetInt("FlowerCount", 0);
        }
	}
	
	// Update is called once per frame
	void FixedUpdate () {
        this.flowerLabel.text = this.currentCount.ToString() + " x";
	}

    public void addFlower() {
        this.currentCount++;
        PlayerPrefs.SetInt("FlowerCount", this.currentCount);
        PlayerPrefs.Save();
        if (!this.GetComponent<AudioSource>().isPlaying)
            this.GetComponent<AudioSource>().Play();
    }
}
